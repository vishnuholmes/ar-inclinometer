package a.r.inc;

import java.io.IOException;

import android.app.Activity;
import android.content.Context;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

public class ARInclinometerActivity extends Activity implements
SensorEventListener, SurfaceHolder.Callback {
	/** Called when the activity is first created. */

	/*Declarations...*/
	Camera camera;
	SurfaceView surfaceView;
	SurfaceHolder surfaceHolder;
	boolean previewing = false;;
	private SensorManager mSensorManager;
	private Sensor mAccelerometer;
	int height;
	int width;
	float fromDegrees;
	float toDegrees;
	Animation anim;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);


		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		height = displaymetrics.heightPixels;	//	Will be useful...
		width = displaymetrics.widthPixels;		//	Somehow, Somewhere...

		surfaceView = (SurfaceView) findViewById(R.id.surfaceView1);	//	Setting up
		surfaceHolder = surfaceView.getHolder();						//	The Surfacview
		surfaceHolder.addCallback(this);								//	For the
		surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);	//	Camera Preview...

		mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);					//
		mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);				//	Yeah... Shake it up...
		mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);	//

		fromDegrees = 0;									//	Angles,...
		toDegrees = 0;										//	Angles are	
		anim = new RotateAnimation(fromDegrees, toDegrees);	//	Attitudes...
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		// TODO Auto-generated method stub	//nothing TODO :D

	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		if(!previewing){
			camera = Camera.open();								//	Start Camera
			if (camera != null){

				try {
					Log.d("dsfdsf", "no exception");
					camera.setPreviewDisplay(surfaceHolder);	//	Action!..
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					Log.d("dsfdsf", "exception");
				}
				camera.startPreview();
				previewing = true;
				// TODO Auto-generated catch block
			}
		}
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		// TODO Auto-generated method stub
		if (event.sensor.getType()==Sensor.TYPE_ACCELEROMETER){
			float ax = event.values[0];
			float ay = event.values[1];
			float az = event.values[2];
			float gravity = (float) Math.sqrt((ax*ax)+(ay*ay)+(az*az));
			float alpha = (float) (Math.acos(ax/gravity)*180/Math.PI);		//
			float beta = (float) (Math.acos(ay/gravity)*180/Math.PI);		//	Oh!.. Nothing Without Physics... 
			float gamma = (float) (Math.acos(az/gravity)*180/Math.PI);		//
			FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(							//
					FrameLayout.LayoutParams.FILL_PARENT, FrameLayout.LayoutParams.FILL_PARENT);//	For the Red Line
			lp.setMargins(0, 0, 0, -(int) (((gamma-90)/90)*width)*2);							//	Go up.. Go Down..
			((ImageView)findViewById(R.id.imageView2)).setLayoutParams(lp);						//

			fromDegrees = toDegrees;
			toDegrees = beta;

			anim = new RotateAnimation(Math.round(fromDegrees), 				//	CON
					Math.round(toDegrees), Animation.RELATIVE_TO_SELF,0.5f,		//	FUS
					Animation.RELATIVE_TO_SELF, 0.5f);							//	ION	

			anim.setDuration(100);


//			findViewById(R.id.imageCrossWire).startAnimation(anim);
			((TextView)findViewById(R.id.textView1)).setText("Angle of elevation = " + Math.round(gamma-90)+"°");
			//			+ "  b = "+beta +"  g = "+gamma );

		}
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		if(camera != null && previewing){
			camera.stopPreview();				//	
			camera.release();					//	CUT!!!...
			camera = null;						//	

			previewing = false;					//	Did we cut?
		}
		super.onStop();
	}
}